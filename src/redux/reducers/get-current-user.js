import { GET_CURRENT_USER } from '../actions/types';
const initialState = {
    data: [],
}
export default function getCurrentUser(state = initialState, action) {
    switch (action.type) {
        case GET_CURRENT_USER:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state
    }
}