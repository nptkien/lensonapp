import { FETCH_CUSTOMER, FETCH_CUSTOMER_SUCCESS, FETCH_CUSTOMER_FAILURE } from '../actions/types';
const initialState = {
  data: [],
  isFetching: false,
  error: false
}

export default function customer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CUSTOMER:
      return {
        ...state,
        data: [],
        isFetching: true
      }
    case FETCH_CUSTOMER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data
      }
    case FETCH_CUSTOMER_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}