import React, { Component } from 'react';
import { TouchableOpacity, Dimensions, Image } from 'react-native';
import { Text } from 'native-base';
import productLogo from '../../../../img/product.png'
import activeLogo from '../../../../img/active.png';
import warrantyLogo from '../../../../img/warranty.png';
import payLogo from '../../../../img/pay.png';
const screen = Dimensions.get('window')

class Services extends Component {
      state = {}
      _renderLogoServices(logoName) {
            switch (logoName) {
                  case 'PRODUCT': {
                        return (<Image source={productLogo} style={styles.image} />)
                  }
                  case 'ACTIVE': {
                        return (<Image source={activeLogo} style={styles.image} />)
                  }
                  case 'WARRANTY': {
                        return (<Image source={warrantyLogo} style={styles.image} />)
                  }
                  case 'PAY': {
                        return (<Image source={payLogo} style={styles.image} />)
                  }
                  default: return null
            }
      }
      render() {
            return (
                  <TouchableOpacity style={styles.container} onPress={this.props.gotoServices}>
                        {this._renderLogoServices(this.props.logoName)}
                        <Text style={{ marginTop: 10 }}>{this.props.title}</Text>
                  </TouchableOpacity>
            );
      }
}

const styles = {
      container: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            borderRightWidth: 1,
            borderBottomWidth: 1,
            backgroundColor: '#fff'
      },
      image: {
            width: 70,
            height: 70
      }

}
export default Services;