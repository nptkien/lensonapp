import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, TextInput, ScrollView, Picker, ActivityIndicator, AsyncStorage } from 'react-native';
import { Text, Toast } from 'native-base';
import { connect } from 'react-redux';
import color from '../../../../ultis/styles/common-css';
import CustomTextInput from '../../../../components/text-input';
import BasicView from '../../../../ultis/styles/AppStyles';
import DatePicker from '../../../../components/date-picker';
import PayHistoryController from './controller';
import ToggleBox from '../toggle-box';
import Detail from './detail';
import currency from 'currency.js';
import ButtonHotline from '../../../../components/button-hotline/ButtonHotline';


class PayHistory extends Component {
    constructor(props) {
        super(props);
        history = new PayHistoryController();

        this.state = {
            isLoading1: false,
            getLength: -1,
            fullName: '',
            phoneNum:'',
            bankName: '',
            bankNum: ''
        }
    }
     _aaa = async () => {
        try {
            const fullName = await AsyncStorage.getItem('fullName');
            const phoneNum = await AsyncStorage.getItem('phoneNum');
            const bankNum = await AsyncStorage.getItem('bankNum');
            const bankName = await AsyncStorage.getItem('bankName');
                this.state.fullName = fullName;
                this.state.phoneNum = phoneNum;
                this.state.bankName =bankName;
                this.state.bankNum = bankNum;
            
        } catch (error) {
            // Error retrieving data
        }
    }
    _startYear() {
        let year = new Date().getFullYear();
        return new Date(year, 0, 0, 0, 0).getTime();
    }
    getStatisticActivationInYear(employeeId, cb) {
        cb.setState({ isLoading1: true })
        fetch((`https://app-dot-casper-electric.appspot.com/api?action=statisticActivation`), {

            method: 'POST',
            body: JSON.stringify({
                startTime: this._startYear(),
                endTime: new Date().getTime(),
                employeeId: employeeId,
                status: 0
            }),
            headers: { "Content-Type": "application/json; charset=utf-8" }
        })
            .then((response) => response.json())
            .then((responJSON) => {
                //this.setResponse2(responJSON.data);
                cb.setState({ isLoading1: false, getLength: responJSON.data.length })
                //console.log('AAA',responJSON.data)
            })
            .catch((error) => { console.log('nhay vao loi', error) })

            ;
    }
    async componentWillMount() {
        await this.getStatisticActivationInYear(this.props.employee, this);
        history.getCurrentMonth();
        this._aaa()
    }
    render() {
        //console.log('ADSF', history.getResponse2())
        return (
            <View style={styles.container}>
                {
                    this.state.isLoading1 && <ActivityIndicator style={{ alignSelf: 'center' }} />
                }
                {
                    (this.state.getLength == 0) ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'red' }}>Bạn chưa được thanh toán lần nào</Text>
                    </View> : null
                }
                <ScrollView style={{ flex: 1 }}>
                    {
                        history.getMonthArr().map((e, i) => {
                            return <DetailRevenuePerMonth
                                e={e}
                                key={i}
                                ref={ref => this.DetailRevenuePerMonth = ref}
                                employee={this.props.employee}
                                navigation={this.props.navigation}
                                fullName={this.state.fullName}
                                phoneNum={this.state.phoneNum}
                                bankName={this.state.bankName}
                                bankNum={this.state.bankNum}
                            />
                        }
                        )
                    }
                </ScrollView>
                <ButtonHotline style={{ marginTop: 10, marginBottom: 10 }} />
            </View>
        );
    }
}

class DetailRevenuePerMonth extends Component {
    constructor(props) {
        super(props);
        history = new PayHistoryController();

        this.state = {
            isLoading: false,
            totalMoney: 0,
            moneyPaid: 0,
            type: 0
        }
        
    }

    componentDidMount() {
        history.getNumberDateInCurrentMonth(this.props.e);
        history.getStatisticActivationInMonth(this.props.employee, this);
        
    }
   
    render() {

        const { e } = this.props;
        let test = history.getResponse();

        if (test.length == 0) {
            return null
        }
        else {
            //console.log('AAa', test)
            return (
                <View>
                    {
                        this.state.isLoading && <ActivityIndicator style={{ alignSelf: 'center' }} />
                    }
                    <ToggleBox
                        label={`Tháng ${e + 1}`}
                        value='Chi Tiết'
                        style={styles.coslapsibleContainer}
                        ref={ref => this.ToggleBox = ref}
                        moneyPaid={currency(this.state.moneyPaid, { separator: ',', precision: 0 }).format()}
                        lastUpdate={history._convertFormatDate(test[0].createdDate)}

                    >
                        <Detail
                            activedDevices={test.length}
                            lastUpdate={history._convertFormatDate(test[0].createdDate)}
                            month={e}
                            test={history.getResponse()}
                            fullName={this.props.fullName}
                            phoneNum={this.props.phoneNum}
                            bankName={this.props.bankName}
                            bankNum={this.props.bankNum}
                        />
                    </ToggleBox>

                </View>
            )
        }

    }
}
const screen = Dimensions.get('window')
const styles = {
    container: {
        flex: 1,
    },
    text: {
        fontSize: 14,
        width: '20%',
        marginLeft: 5,
    },

    searchButton: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: '25%',
        backgroundColor: color.colorBlue,
        borderRadius: 4,
        alignSelf: 'center'
    },
    viewDatePicker: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 10,
        flexDirection: 'row',

    },
    viewPicker: {
        borderWidth: 0.5,
        borderRadius: 4,
        width: '50%',
        height: 40
    },
    coslapsibleContainer: {
        backgroundColor: '#fff',
        marginTop: 10,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
}
const mapSateToProps = state => {
    return {
        employee: state.getCurrentUser.data,
        isFetching: state.getCurrentUser.isFetching
    }
}
export default connect(mapSateToProps)(PayHistory);