export default class PayHistoryController {
  _fullName = '';
  _phoneNum = '';
  _bankName = '';
  _bankNum = '';
  _activedDevices = 0;
  _paidDate = '';
  _note = '';
  _startMonth = 0;
  _endMonth = 0;
  _monthArr = [];
  _response = [];
  _response2 = [];
  getResponse() {
    return this._response;
  };
  getResponse2() {
    return this._response2;
  };
  getStartMonth() {
    return this._startMonth;
  };
  getEndMonth() {
    return this._endMonth;
  };
  getFullName() {
    return this._fullName;
  };
  getPhoneNum() {
    return this._phoneNum;
  };
  getBankName() {
    return this._bankName;
  };
  getBankNum() {
    return this._bankNum;
  };
  getPaidDate() {
    return this._paidDate;
  };
  getNote() {
    return this._note;
  };

  setFullName(fullName) {
    this._fullName = fullName;
  };
  setPhoneNum(phoneNum) {
    this._phoneNum = phoneNum;
  };
  setBankName(name) {
    this._bankName = name;
  };
  setBankNum(bankNum) {
    this._bankNum = bankNum;
  };
  setActivedDevices(devices) {
    this._activedDevices = devices;
  };
  setPaidDate(date) {
    this._paidDate = date;
  };
  setNote(note) {
    this._note = note;
  };
  getMonthArr() {
    return this._monthArr;
  };
  setMonthArr(arr) {
    this._monthArr = arr;
  };
  setStartMonth(month) {
    this._startMonth = month;
  };
  setEndMonth(month) {
    this._endMonth = month;
  };
  setResponse(response) {
    this._response = response;
  };
  setResponse2(response2) {
    this._response2 = response2;
  };

  _convertFormatDate(value) {
    let year = new Date(value).getFullYear();
    let month = new Date(value).getMonth() + 1;
    let date = new Date(value).getDate();
    return `${date}/${month}/${year}`
  }
  _totalValue() {
    var money = 0;
    for (let i = 0; i < history.getResponse().length; i++) {
      money = money + history.getResponse()[i].moneyEmployeeRecent;

    }
    return money;
  }
  _getActivationPaid() {
    var moneyPaid = 0;
    for (let i = 0; i < history.getResponse().length; i++) {
      if (history.getResponse()[i].status == 0) {
        moneyPaid = moneyPaid + history.getResponse()[i].moneyEmployeeRecent;
      }
    }
    return moneyPaid;
  }
  getNumberDateInCurrentMonth(month) {
    let year = new Date().getFullYear();
    this.setStartMonth(new Date(year, month, 1, 0, 0, 0, 0).getTime());
    this.setEndMonth(new Date(year, month + 1, 1).getTime() - 1);
  }
  getCurrentMonth() {
    let month = new Date().getMonth() + 1;
    let temp = [];
    for (let i = month; i > 0; i --) {
      temp.push(i)
    }
    this.setMonthArr(temp)
    return temp
  }
  compare(a, b) {
    const genreA = a.createdDate;
    const genreB = b.createdDate;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    console.log(comparison)
    return comparison * -1;
  }
  async getStatisticActivationInMonth( employeeId, cb) {
      cb.setState({ isLoading: true })
    await fetch((`https://app-dot-casper-electric.appspot.com/api?action=statisticActivation`), {

      method: 'POST',
      body: JSON.stringify({
        startTime: this.getStartMonth(),
        endTime: this.getEndMonth(),
        employeeId: employeeId,
        status: 0
      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {
        this.setResponse(responJSON.data);
        console.log('ADF',this.getResponse())
        cb.setState({ isLoading: false, totalMoney: this._totalValue(), moneyPaid: this._getActivationPaid() })
      })
      .catch((error) => { console.log('nhay vao loi', error) })

      ;
  }
  
}