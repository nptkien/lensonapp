import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as ActionCreator from '../../../../redux/actions';
import color from '../../../../ultis/styles/common-css';
import DatePicker from '../../../../components/date-picker';
class Option extends Component {
     constructor(props) {
          super(props);
          this.state = {
               headerText: 'Tra cứu doanh số kích hoạt theo ngày hoặc theo trạng thái',
               fromDate: '',
               toDate: ''
          }
     }
     convertDateInput(value) {
          return new Date(value).getTime()
     }
     render() {
          
          return (
               <View style={styles.container}>
                    <Text style={styles.header}>{this.state.headerText}</Text>
                    <View style={styles.viewDatePicker}>
                         <Text style={styles.text}>Ngày bắt đầu</Text>
                         <DatePicker
                              title={'Từ ngày...'}
                              showIcon={false}
                              style={{ width: '100%' }}
                              getValue={(date) => { this.setState({ fromDate: this.convertDateInput(date) }) }}
                         />
                         <Text style={styles.text}>Ngày kết thúc</Text>
                         <DatePicker
                              title={'Đến ngày...'}
                              showIcon={false}
                              style={{ width: '100%' }}
                              getValue={(date) => { this.setState({ toDate: this.convertDateInput(date) + 24 * 60 * 60 * 1000 }) }}
                         />
                    </View>
                    <TouchableOpacity style={styles.button} onPress={async () => { 
                         this.props.navigation.navigate('DrawerClose');
                         await this.props.getList(`https://app-dot-casper-electric.appspot.com/api?action=statisticActivation`, this.state.fromDate, this.state.toDate,this.props.employeeId.account, -1, this) }}>
                         <Text style={[styles.text, { fontSize: 16 }]}>Tra cứu</Text>
                    </TouchableOpacity>
               </View>
          );
     }
}
const styles = {
     container: {
          flex: 1,
          marginTop: 20,
          backgroundColor: color.colorBlue,
          paddingLeft: 5
     },
     header: {
          alignSelf: 'center',
          fontSize: 14,
          marginTop: 50,
          fontWeight: 'bold',
          color: color.colorWhite,

     },
     viewDatePicker: {
          width: '100%',
          justifyContent: 'space-around',
          marginTop: 40,
          height: 150,

     },
     text: {
          color: color.colorWhite
     },
     button: {
          width: "90%",
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 70,
          backgroundColor: color.colorOrange,
          marginTop: 80,
          borderRadius: 10
     }
}
const mapStateToProps = state => {
     return {
          employeeId: state.getCurrentUser.data,
          aaa: state.getList.data,
          isFetching: state.getList.isFetching
     }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getList: (url,startTime, endTime, employeeId, status, cb) => {
            dispatch(ActionCreator.fetchCustomer2(url, startTime, endTime, employeeId, status, cb))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Option);