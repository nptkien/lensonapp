//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, AsyncStorage, ActivityIndicator, Keyboard } from 'react-native';
import Header from '../../../components/header/Header';
import TextInput from '../../../components/custom-textinput/CustomTextInput';
import { connect } from 'react-redux';
import { Toast } from 'native-base';
import CryptoJS from 'crypto-js';
import md5 from '../../../libs/md5/md5';
// create a component
class ChangePassword extends Component {
    constructor(props) {
        super(props);
        password = '';
        repassword = '';
        disableOnpress = false;
        this.state = {
            isLoading: false
        }
    }
    handleFocus(nextInput) {
        this.refs[nextInput].text.focus();
    }
    encryptorPass(userName, password) {
        let value = md5(userName + '.' + password)
        let keyHex = CryptoJS.enc.Utf8.parse(value);
        var encrypted = CryptoJS.DES.encrypt(password, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted
    }
    handleChangePass = async () => {
        try {
            const account = await AsyncStorage.getItem('account');
            if (this.props.isOnline === true) {
                fetch('https://app-dot-casper-electric.appspot.com/api?action=changePassWord',
                    {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            account: account,
                            newPassWord: this.encryptorPass(account, password).toString(),
                        })
                    }

                )
                    .then((response) => response.json())
                    .then((responseJson) => {
                        disableOnpress = false;
                        this.setState({
                            isLoading: false
                        });
                        this.showToast("Đổi mật khẩu thành công!")
                    })
                    .catch((error) => {

                    });
            } else {
                this.showToast("Không có kết nối mạng!")
            }

        } catch (error) {

        }
    }
    changePassword() {
        Keyboard.dismiss();
        if (password === '') {
            this.showToast("Nhập mật khẩu trống");
            return;
        }
        if (repassword === '') {
            this.showToast("Nhập lại mật khẩu trống");
            return;
        }
        if (password !== repassword) {
            this.showToast("Mật khẩu không khớp");
            return;
        }
        if (disableOnpress === false) {
            disableOnpress = true;
            this.setState({
                isLoading: true
            })
            this.handleChangePass();
        }
    }
    showToast(text) {
        Toast.show({
            text: text,
            position: 'top',
            duration: 2000
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    title={'Đổi mật khẩu'}
                    navigation={this.props.navigation}
                />
                <View style={styles.preTextInput}>
                    <TextInput
                        label={'Nhập mật khẩu mới'}
                        onSubmitEditing={(e) => {
                            this.handleFocus(1);
                        }}
                        returnKeyType={'next'}
                        onChangeText={(e) => password = e}
                    />
                    <TextInput
                        label={'Nhập lại mật khẩu'}
                        onChangeText={(e) => repassword = e}
                        secureTextEntry
                        ref='1'
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    {this.state.isLoading && <ActivityIndicator />}
                </View>
                <TouchableOpacity
                    style={styles.btnLogin}
                    onPress={() => this.changePassword()}
                >
                    <Text style={[styles.textStyle, { color: 'white' }]}>Gửi</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    preTextInput: {
        width: '100%',
        height: 160,
        paddingLeft: 15,
        paddingRight: 15,
        alignSelf: 'center',
        marginTop: 40,
        justifyContent: 'space-around',
    },
    btnLogin: {
        width: '90%',
        height: 50,
        alignSelf: 'center',
        borderRadius: 5,
        backgroundColor: '#4493AA',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    textStyle: {
        fontSize: 16,
    },
});
const mapStateToProps = (state, ownProps) => {
    return {
        isOnline: state.getOnline.data,
    }
}
export default connect(mapStateToProps)(ChangePassword);
//make this component available to the app
