import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Linking, ScrollView } from 'react-native';
import { Text, Icon } from 'native-base';
import BasicView from '../../../ultis/styles/AppStyles';
import color from '../../../ultis/styles/common-css';
// import local icon ....
import account from '../../../../img/account.png'
import product from '../../../../img/product.png'
import active from '../../../../img/active.png'
import warranty from '../../../../img/warranty.png'
import pay from '../../../../img/pay.png'
import logout from '../../../../img/logout.png'
class Menu extends Component {
    state = {}
    render() {
        let listmenu = {
            userInfo: 'Thông tin tài khoản',
            productInfo: 'Thông tin chung',
            activeInfo: 'Kích hoạt',
            warrantyInfo: 'Bảo hành',
            paymentInfo: 'Doanh thu - Thanh toán',
            logout: 'Đăng xuất',
        }
        const { navigate } = this.props.navigation
        return (
            <ScrollView style={styles.container}>
                <View style={styles.avatarContainer}>
                    <Image source={account} style={styles.imageAvatar} resizeMode={'center'} />
                    <TouchableOpacity
                        style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}
                        onPress={this.props.nextChangePass}
                    >
                        <Text style={[styles.textMenu, { marginRight: 10 }]}>{this.props.fullName}</Text>
                        <Icon name='create' style={{ fontSize: 20, color: '#666666' }} />
                    </TouchableOpacity>
                </View>

                <Item
                    source={product}
                    text={listmenu.productInfo}
                    onPress={() => { Linking.openURL('http://casper-electric.com/') }}
                />
                <Item
                    source={active}
                    text={listmenu.activeInfo}
                    onPress={() => navigate('Active', { lastActivation: { id: -1 } })}
                />

                <Item
                    source={pay}
                    text={listmenu.paymentInfo}
                    onPress={() => { navigate('Payment') }}
                />
                <Item
                    source={logout}
                    text={listmenu.logout}
                    onPress={this.props.onPressLogout}
                />
            </ScrollView>
        );
    }
}
class Item extends Component {
    render() {
        return (
            <TouchableOpacity
                style={[BasicView.horizontalView, styles.listMenu]}
                onPress={this.props.onPress}
            >
                <Image source={this.props.source} style={styles.image} resizeMode={'contain'} />
                <Text style={[styles.textMenu, { marginTop: 0, fontSize: 17 }]}>{this.props.text}</Text>
            </TouchableOpacity>
        )
    }
}
const styles = {
    container: {
        height: '100%',
        backgroundColor: 'white'
    },
    avatarContainer: {
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15,
        backgroundColor: '#f2f2f2'
    },
    image: {
        width: '10%',
        height: '100%',
        marginLeft: 10,
        marginRight: 20
    },
    listMenu: {
        alignItems: 'center',
        height: 50
    },
    textMenu: {
        color: '#666666',
        fontSize: 20,

    },
    imageAvatar: {
        width: 100,
        height: 100,
        borderRadius: 50,

    }
}
export default Menu;