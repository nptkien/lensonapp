import React, { Component } from 'react';
import { View, TextInput, Dimensions, TouchableOpacity, Image, Text } from 'react-native';
import color from '../../ultis/styles/common-css';

const screen = Dimensions.get('window')
class CustomTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }
    render() {
        return (
            <View style={{marginBottom:5,...this.props.styleParent}}>
                {
                    (this.props.textTitle)
                        ?
                        <Text style={[styles.textTitle,{...this.props.styleTitle}]}>{this.props.textTitle}</Text>
                        :
                        null
                }
                <TextInput
                    onChangeText={(text) => this.setState({ value: text })}
                    value={this.state.fullName}
                    style={[styles.container, { ...this.props.style }]}
                    underlineColorAndroid={'transparent'}
                    placeholder={this.props.placeholder}
                    onSubmitEditing={this.props.onSubmitEditing}
                />
                {
                    (this.props.imageIcon) ?
                        <TouchableOpacity
                            onPress={this.props.onPressIcon}
                            style={styles.onPressIcon}
                        >
                            <Image
                                style={styles.icon}
                                source={this.props.source}
                                resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                        :
                        null
                }
            </View>
        );
    }
}


const styles = {
    container: {
        height: 40,
        width: screen.width - 20,
        borderWidth: 1,
        backgroundColor: 'white',
        borderRadius: 5,
        margin: 5,
        padding: 10
    },
    textTitle:{
        color:'black',
        marginLeft:30
    },
    icon: {
        width: 27,
        height: 27,
        alignSelf: 'center'
    },
    onPressIcon: {
        position: 'absolute',
        right: 15,
        top: 10,
        width: 30,
        height: 30,
    }

}
export default CustomTextInput;