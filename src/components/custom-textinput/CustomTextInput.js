//import liraries
import React, { Component } from 'react';
import Madoka from '../makoda/Madoka';
import colorApp from '../../ultis/styles/common-css';
// create a component
export default class TextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maxLength: 100
        }
    }

    lengthText(e) {
        if (!this.props.typePhone) return;
        if (e.startsWith("09")) {
            this.setState({
                maxLength: 10
            })
            return;
        }
        if (e.startsWith("01")) {
            this.setState({
                maxLength: 11
            })
            return;
        }
        if (e.startsWith("849")) {
            this.setState({
                maxLength: 11
            })
            return;
        }
        if (e.startsWith("841")) {
            this.setState({
                maxLength: 12
            })
            return;
        }
    }
    getKeyboardType() {
        if (this.props.typePhone) return 'phone-pad';
        if (this.props.typeEmail) return 'email-address';
        return 'default';
    }
    render() {
        return (
            <Madoka
                {...this.props}
                label={this.props.label}
                // this is used as active and passive border color
                borderColor={colorApp.colorBlue}
                labelStyle={{ color: colorApp.colorBlue, fontWeight: 'normal' }}
                inputStyle={{ fontWeight: 'normal' }}
                note={this.props.note}
                noteStyle={this.props.noteStyle}
                onSubmitEditing={(e) => {
                    if (this.props.onSubmitEditing !== undefined) {
                        this.props.onSubmitEditing(e)
                    }
                    if (this.props.onEnter !== undefined) {
                        this.props.onEnter();
                    }
                }}
                onChangeText={(e) => {
                    this.lengthText(e);
                    if (this.props.onChangeText !== undefined)
                        this.props.onChangeText(e)
                }}
                maxLength={this.state.maxLength}
                keyboardType={this.getKeyboardType()}
                ref={(ref) => this.text = ref}
                returnKeyType={this.props.returnKeyType}
            />
        );
    }
}


